#include <iostream>
using namespace std;

int main()
{
    int a = 4;
    int b = 5;

    // Arithmetic operators:
    cout << "\nArithmetic operators:-" << endl;
    cout << "a + b = " << (a + b) << endl;
    cout << "a - b = " << (a - b) << endl;
    cout << "a * b = " << (a * b) << endl;
    cout << "a / b = " << (a / b) << endl;
    cout << "a % b = " << (a % b) << endl;
    cout << "a++ = " << (a++) << endl; // after this line a = 5
    cout << "b-- = " << (b--) << endl; // after this line b = 3
    // ++a -> increment and print
    // a++ -> print and increment

    // Assignment operators: used assign values to variables
    cout << "\nAssignment operators:-" << endl;
    char t = 'd';
    float gpa = 8.52;
    cout << t << endl;
    cout << gpa << endl;

    // Comparison operators
    cout << "\nComparison operators:-" << endl;
    cout << "a = b? " << (a == b) << endl;
    cout << "a > b? " << (a > b) << endl;
    cout << "a < b? " << (a < b) << endl;
    cout << "a >= b? " << (a >= b) << endl;
    cout << "a <= b? " << (a <= b) << endl;
    cout << "a != b? " << (a != b) << endl;

    // Logical operators:
    cout << "\nLogical operators:-" << endl;
    cout << "The value of OR logical operator is: " << ((a == b) || (a < b)) << endl;
    cout << "The value of AND logical operator is: " << ((a == b) && (a < b)) << endl;
    cout << "The value of NOT logical operator is: " << ((a == b) != (a < b)) << endl;
}
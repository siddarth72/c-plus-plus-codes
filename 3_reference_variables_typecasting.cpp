#include <iostream>
using namespace std;

// Global variable c
int c = 72;

int main()
{
    // Literals
    cout << "\nLiterals:- " << endl;

    int a, b, c;
    cout << "Enter a: ";
    cin >> a;
    cout << "Enter b: ";
    cin >> b;
    c = a + b;
    cout << "a + b = " << c << endl;     // takes local variable
    cout << "Global c: " << ::c << endl; // takes Global variable

    float d = 34.4f;       // 34.4F
    long double e = 34.4l; // 34.4L
    // 34.4 -> it is by default double
    // 34.4f -> it is by default float
    // 34.4l -> it is by default long double
    cout << "The value of d is " << d << endl;
    cout << "The value of e is " << e << endl;

    cout << "\nSize(64-bit system) :-" << endl;
    cout << "The size of 34.4 is: " << sizeof(34.4) << endl;
    cout << "The size of 34.4f is: " << sizeof(34.4f) << endl;
    cout << "The size of 34.4F is: " << sizeof(34.4F) << endl;
    cout << "The size of 34.4l is: " << sizeof(34.4l) << endl;
    cout << "The size of 34.4L is: " << sizeof(34.4L) << endl;

    // Reference variables
    cout << "\nReference variables:- " << endl;

    float x = 455;
    float &y = x; // y is referencing to x
    cout << "x: " << x << endl;
    cout << "y: " << y << endl;

    // Type casting
    cout << "\nType casting:-" << endl;

    int z = 45;
    cout << "a = " << (float)z << endl;

    float p = 45.56;
    cout << "b = " << (int)p << endl;

    cout << "The expression is: " << z + p << endl;
    cout << "The expression is: " << z + int(p) << endl;
    cout << "The expression is: " << z + (int)p << endl;

    return 0;
}
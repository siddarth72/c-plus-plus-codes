#include <iostream> // system header file
using namespace std;

// There are 2 types of header files
// 1. System header files - comes with the compiler
// 2. User defined header files
// #include "this.h" -> syntax

int glo = 6;

void printNum()
{
    cout << "Main glo: " << glo << endl;
}

int main()
{
    // Variables
    int num = 9;
    cout << "Number = " << num << endl;

    // Data types
    int a = 4;
    int b = 5;
    float pi = 3.14;
    char ch = 'd';
    bool isTru = true;

    // Local variables
    int glo = 56;
    glo = 78;
    cout << "Local glo: " << glo << endl;

    // Input
    int p, q;
    cout << "Enter a: "; // Insertion operator
    cin >> p;            // Extraction operator
    cout << "Enter b : ";
    cin >> q;
    cout << "Sum is: " << (p + q) << endl;

    return 0;
}
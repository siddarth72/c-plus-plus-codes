#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    // Constants
    cout << "\nConstants:- " << endl;

    const int a = 34;
    // a = 45; -> cannot override
    cout << "a = " << a << endl;

    // Manipulators - control display of program output
    cout << "\nManipulators:- " << endl;
    // Ex: endl: next line
    int b = 3, c = 78, d = 1233;

    cout << "b = " << b << endl;
    cout << "c = " << c << endl;
    cout << "d = " << d << endl;

    cout << "\nusing setw:- " << endl;
    cout << "b = " << setw(4) << b << endl;
    cout << "c = " << setw(4) << c << endl;
    cout << "d = " << setw(4) << d << endl;

    // Operator Precedence
    cout << "\nOperator Precedence:- " << endl;

    int p = 3, q = 4;
    // int r = p * 5 + q;
    int r = ((((p * 5) + q) - 45) + 87);
    cout << r << endl;

    return 0;
}